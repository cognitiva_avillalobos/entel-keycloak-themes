<#outputformat "plainText">
<#assign requiredActionsText><#if requiredActions??><#list requiredActions><#items as reqActionItem>${msg("requiredAction.${reqActionItem}")}<#sep>, </#sep></#items></#list></#if></#assign>
</#outputformat>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
      body {width: 600px;margin: 0 auto;}
      table {border-collapse: collapse;}
      table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
      img {-ms-interpolation-mode: bicubic;}
    </style>
    <![endif]-->

    <style type="text/css">
      body, p, div {
        font-family: arial,helvetica,sans-serif;
        font-size: 13px;
      }
      body {
        color: #595a5d;
      }
      body a {
        color: #0075be;
        text-decoration: none;
      }
      p { margin: 0; padding: 0; }
      table.wrapper {
        width:100% !important;
        table-layout: fixed;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: 100%;
        -moz-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
      }
      img.max-width {
        max-width: 100% !important;
      }
      .column.of-2 {
        width: 50%;
      }
      .column.of-3 {
        width: 33.333%;
      }
      .column.of-4 {
        width: 25%;
      }
      @media screen and (max-width:480px) {
        .preheader .rightColumnContent,
        .footer .rightColumnContent {
            text-align: left !important;
        }
        .preheader .rightColumnContent div,
        .preheader .rightColumnContent span,
        .footer .rightColumnContent div,
        .footer .rightColumnContent span {
          text-align: left !important;
        }
        .preheader .rightColumnContent,
        .preheader .leftColumnContent {
          font-size: 80% !important;
          padding: 5px 0;
        }
        table.wrapper-mobile {
          width: 100% !important;
          table-layout: fixed;
        }
        img.max-width {
          height: auto !important;
          max-width: 480px !important;
        }
        a.bulletproof-button {
          display: block !important;
          width: auto !important;
          font-size: 80%;
          padding-left: 0 !important;
          padding-right: 0 !important;
        }
        .columns {
          width: 100% !important;
        }
        .column {
          display: block !important;
          width: 100% !important;
          padding-left: 0 !important;
          padding-right: 0 !important;
        }
      }
    </style>
    <!--user entered Head Start-->
    
     <!--End Head user entered-->
  </head>
  <body>
    <center class="wrapper" data-link-color="#0075be" data-body-style="font-size: 13px; font-family: arial,helvetica,sans-serif; color: #595a5d; background-color: #ffffff;">
      <div class="webkit">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
          <tr>
            <td valign="top" bgcolor="#ffffff" width="100%">
              <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td width="100%">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td>
                          <!--[if mso]>
                          <center>
                          <table><tr><td width="600">
                          <![endif]-->
                          <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                            <tr>
                              <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #595a5d; text-align: left;" bgcolor="#f8f8f8" width="100%" align="left">
                                
    <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%"
           style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
      <tr>
        <td role="module-content">
          <p></p>
        </td>
      </tr>
    </table>
  
    <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="font-size:6px;line-height:10px;padding:20px 0px 20px 20px;background-color:#ffffff;" valign="top" align="left">
          <a href="https://www.cognitiva.la/"><img class="max-width" width="129" height="30" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;" src="https://marketing-image-production.s3.amazonaws.com/uploads/4517dcf0f5d210bf61aac69cb643a5a738c47089c4adeb8ead97d56fbbab4542d9df031935ff8c70094e0b2f5bff508738db40647a81388b5199fa572de610f5.png" alt="Logo Cognitiva "></a>
        </td>
      </tr>
    </table>
  
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="background-color:#0075be;padding:20px 0px 20px 20px;line-height:22px;text-align:inherit;"
            height="100%"
            valign="top"
            bgcolor="#0075be">
            <div>
<style type="text/css">.sg-campaigns p.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 16.0px Arial; color: #ffffff}
</style>
<p class="p1" style="text-align: left;"><span style="font-size:16px;"><strong><span style="font-family:arial,helvetica,sans-serif;"><span style="color:#FFFFFF;">Restablecer contraseña</span></span></strong></span></p>
</div>
        </td>
      </tr>
    </table>
  
    <table class="module"
           role="module"
           data-type="divider"
           border="0"
           cellpadding="0"
           cellspacing="0"
           width="100%"
           style="table-layout: fixed;">
      <tr>
        <td style="padding:0px 0px 0px 0200px;"
            role="module-content"
            height="100%"
            valign="top"
            bgcolor="">
          <table border="0"
                 cellpadding="0"
                 cellspacing="0"
                 align="center"
                 width="100%"
                 height="6px"
                 style="line-height:6px; font-size:6px;">
            <tr>
              <td
                style="padding: 0px 0px 6px 0px;"
                bgcolor="#f4c400"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="padding:20px 0px 20px 020px;line-height:26px;text-align:inherit;"
            height="100%"
            valign="top"
            bgcolor="">
            <div>
<style type="text/css">.sg-campaigns p.p1 {margin: 0.0px 0.0px 0.0px 0.0px; line-height: 20.0px; font: 13.0px Arial; color: #6c6d70}
.sg-campaigns span.s1 {text-decoration: underline ; color: #0075be}
</style>
<p class="p1">
<style type="text/css">.sg-campaigns p.p1 {margin: 0.0px 0.0px 0.0px 0.0px; line-height: 20.0px; font: 13.0px Arial; color: #6c6d70}
</style>
</p>

<p class="p1" style="text-align: left;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Hola,<span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; white-space: pre;"></span></p>

<p class="p1" style="text-align: left;">&nbsp;</p>

<p class="p1" style="text-align: left;"><span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;">Recibimos tu solicitud para reestablecer tu contraseña.</span></span></p>

<p class="p1" style="text-align: left;"><span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;">Durante las próximas 24 horas, utiliza este <span class="s1" href=${msg(link)?no_esc} >enlace de recuperación</span> para lograr acceso de nuevo a tu cuenta.</span></span></p>
</div>
        </td>
      </tr>
    </table>
  <table border="0" cellpadding="0" cellspacing="0" class="module" data-role="module-button" data-type="button" role="module" style="table-layout:fixed;" width="100%"><tbody><tr><td align="center" class="outer-td" style="padding:0px 0px 0px 0px;"><table border="0" cellpadding="0" cellspacing="0" class="button-css__deep-table___2nYCc wrapper-mobile" style="text-align:center;"><tbody><tr><td align="center" bgcolor="#0075be" class="inner-td" style="border-radius:6px;font-size:16px;text-align:center;background-color:inherit;"><a style="background-color:#0075be;border:1px solid #333333;border-color:#333333;border-radius:6px;border-width:0px;color:#ffffff;display:inline-block;font-family:arial,helvetica,sans-serif;font-size:16px;font-weight:bold;letter-spacing:0px;line-height:24px;padding:12px 18px 12px 18px;text-align:center;text-decoration:none;" href=${msg(link)?no_esc} target="_blank">Restablecer </a></td></tr></tbody></table></td></tr></tbody></table>
    <table class="module"
           role="module"
           data-type="spacer"
           border="0"
           cellpadding="0"
           cellspacing="0"
           width="100%"
           style="table-layout: fixed;">
      <tr>
        <td style="padding:0px 0px 30px 0px;"
            role="module-content"
            bgcolor="">
        </td>
      </tr>
    </table>
  
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="background-color:#ffffff;padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;"
            height="100%"
            valign="top"
            bgcolor="#ffffff">
            <div>
<style type="text/css">.sg-campaigns p.p1 {margin: 0.0px 0.0px 0.0px 0.0px; text-align: center; font: 12.0px Arial; color: #6c6d70}
</style>
<p class="p1"><span style="font-size:12px;"><span style="font-family:arial,helvetica,sans-serif;">Por seguridad, evita palabras comunes o tu fecha de cumpleaños, las combinaciones alfanuméricas son las más difíciles de descubrir.</span></span></p>

<p class="p1">&nbsp;</p>

<p class="p1"><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">
<style type="text/css">.sg-campaigns p.p1 {margin: 0.0px 0.0px 0.0px 0.0px; text-align: center; font: 12.0px Arial; color: #6c6d70}
</style>
</span></span></p>

<p class="p1"><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">¿No realizaste esta solicitud? Ignora este mensaje y continúa disfrutando de tu día.</span></span></p>

<p class="p1">&nbsp;</p>

<p class="p1"><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">
<style type="text/css">.sg-campaigns p.p1 {margin: 0.0px 0.0px 0.0px 0.0px; text-align: center; font: 12.0px Arial; color: #6c6d70}
.sg-campaigns span.s1 {text-decoration: underline ; color: #0075be}
</style>
</span></span></p>

<p class="p1"><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:12px;">© 2018 Cognitiva&nbsp; | <span class="s1"><a href="mailto:soporte@cognitiva.la?Subject=Soporte-Asistente-Virtual-Inteligente">soporte@cognitiva.la</a></span></span></span></p>  
</div>
        </td>
      </tr>
    </table>
  
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="background-color:#ffffff;padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;" 
            height="100%"
            width="10%"
            valign="top"
            bgcolor="#ffffff"></td>
        <td style="background-color:#ffffff;padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;" 
            height="100%"
            width="10%"
            valign="top"
            bgcolor="#ffffff"></td>
        <td style="background-color:#ffffff;padding:18px 0px 18px 0px;line-height:22px;text-align:center;"
            height="100%"
            width="15%"
            valign="top"
            bgcolor="#ffffff">
          <p class="p1">
            <a href="https://www.facebook.com/cognitivala"><img class="max-width" width="9" height="16" border="0" src="https://marketing-image-production.s3.amazonaws.com/uploads/4d94e084df2c1a191dfff5b6235feedac221f164b5331bb469bba0d59b12a7617ff1567bed5c3243310cee1468a87406626fb8d67c42ec848cd257a63d277aeb.png" alt="Facebook"></a>
          </p>
        </td>
        <td style="background-color:#ffffff;padding:18px 0px 18px 0px;line-height:22px;text-align:center;"
            height="100%"
            width="15%"
            valign="top"
            bgcolor="#ffffff">
            <p class="p1">
              <a href="https://twitter.com/cognitiva_la"><img class="max-width" width="15" height="12" border="0" src="https://marketing-image-production.s3.amazonaws.com/uploads/6169439b950cfb65c016c6654531bbfd1bbb12e37d27703bc4c7496ce2e766ca351de2afa9985a47b4119d62c549c7c3b4df432439d8c7812672a0c2accf9c76.png" alt="Twitter"></a>
            </p>
        </td>
        <td style="background-color:#ffffff;padding:18px 0px 18px 0px;line-height:22px;text-align:center;"
            height="100%"
            width="15%"
            valign="top"
            bgcolor="#ffffff">
          <p class="p1">
            <a  href="https://www.linkedin.com/company/10441537/"><img class="max-width" width="14" height="14" border="0" src="https://marketing-image-production.s3.amazonaws.com/uploads/d7451ed3a63a76ba62317b32fef2ea8156311efab6bc55feb580740ce471a4c4885273a68a8f617bb680224acb6034213d10a5d20baa520ff72ad90ffbbb307c.png" alt="LinkedIn"></a>
          </p>
        </td>
        <td style="background-color:#ffffff;padding:18px 0px 18px 0px;line-height:22px;text-align:center;"
            height="100%"
            width="15%"
            valign="top"
            bgcolor="#ffffff">
          <p class="p1">
            <a href="https://www.youtube.com/channel/UCnjc5DAAWJPojKfQhYMBTcw"><img class="max-width" width="14" height="17" border="0" src="https://marketing-image-production.s3.amazonaws.com/uploads/d136a2297fac55aba5709e354652ba12ff281932832de1274d955e77d2954f621a5f7a72ab34555feb32393a01bda68a42b8b2916a7890fbd912ba19c080b1b9.png" alt="Youtube"></a>
          </p>
        </td>
        <td style="background-color:#ffffff;padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;" 
            height="100%"
            width="10%"
            valign="top"
            bgcolor="#ffffff"></td>
        <td style="background-color:#ffffff;padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;" 
            height="100%"
            width="10%"
            valign="top"
            bgcolor="#ffffff"></td>
      </tr>
    </table>
                              </td>
                            </tr>
                          </table>
                          <!--[if mso]>
                          </td></tr></table>
                          </center>
                          <![endif]-->
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </center>
  </body>
</html>
