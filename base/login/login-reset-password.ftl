<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
    <#if section = "title">
        ${msg("emailForgotTitle")}
    <#elseif section = "header">
        ${msg("emailForgotTitle")}
    <#elseif section = "form">
        <div class="row">
            <div class="col-md-12 cognitiva-subtitle">Escribe tu correo electrónico</div> 
        </div>
        <form id="kc-reset-password-form" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">
            <div class="${properties.kcFormGroupClass!}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="username" class="${properties.kcLabelClass!}" style="font-weight: 400;"><#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if></label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px;">
                    <input type="text" id="username" name="username" class="${properties.kcInputClass!}" autofocus/>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 submit" style="text-align: left;">
                    <span><a href="${url.loginUrl}">${msg("backToLogin")?no_esc}</a></span>
                </div>
                <div id="kc-form-buttons" class="col-xs-6 col-sm-6 col-md-6 col-lg-6 submit" style="padding-right: 0;">
                    
                        <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg("doSubmit")}" style="float: right; font-weight: 400 !important;"/>
                   
                </div>
            </div>
        </form>
    <#elseif section = "info" >
        ${msg("emailInstruction")}
    </#if>
</@layout.registrationLayout>
