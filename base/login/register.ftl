<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "title">
        ${msg("registerWithTitle",(realm.displayName!''))}
    <#elseif section = "header">
        ${msg("registerWithTitleHtml",(realm.displayNameHtml!''))?no_esc}
    <#elseif section = "form">
        <div class="row">
            <div class="col-md-12 cognitiva-subtitle">Crea una nueva cuenta</div>
        </div>
        <form id="kc-register-form" class="${properties.kcFormClass!}" action="${url.registrationAction}" method="post">
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('firstName',properties.kcFormGroupErrorClass!)}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="firstName" class="${properties.kcLabelClass!}" style="font-weight: 400;">${msg("firstName")}</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px;">
                    <input type="text" id="firstName" class="${properties.kcInputClass!}" name="firstName" value="${(register.formData.firstName!'')}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('lastName',properties.kcFormGroupErrorClass!)}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="lastName" class="${properties.kcLabelClass!}" style="font-weight: 400;">${msg("lastName")}</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px;">
                    <input type="text" id="lastName" class="${properties.kcInputClass!}" name="lastName" value="${(register.formData.lastName!'')}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('email',properties.kcFormGroupErrorClass!)}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="email" class="${properties.kcLabelClass!}" style="font-weight: 400;">${msg("email")}</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px;">
                    <input type="text" id="email" class="${properties.kcInputClass!}" name="email" value="${(register.formData.email!'')}" autocomplete="email" />
                </div>
            </div>

          <#if !realm.registrationEmailAsUsername>
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('username',properties.kcFormGroupErrorClass!)}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="username" class="${properties.kcLabelClass!}" style="font-weight: 400;">${msg("username")}</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px;">
                    <input type="text" id="username" class="${properties.kcInputClass!}" name="username" value="${(register.formData.username!'')}" autocomplete="username" />
                </div>
            </div>
          </#if>

            <#if passwordRequired>
            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('password',properties.kcFormGroupErrorClass!)}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="password" class="${properties.kcLabelClass!}" style="font-weight: 400;">${msg("password")}</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px;">
                    <input type="password" id="password" class="${properties.kcInputClass!}" name="password" autocomplete="new-password" autocomplete="off"/>
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!} ${messagesPerField.printIfExists('password-confirm',properties.kcFormGroupErrorClass!)}">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="password-confirm" class="${properties.kcLabelClass!}" style="font-weight: 400;">${msg("passwordConfirm")}</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px;">
                    <input type="password" id="password-confirm" class="${properties.kcInputClass!}" name="password-confirm" autocomplete="off"/>
                </div>
            </div>
            </#if>

            <div class="form-group">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="user.attributes.company" class="${properties.kcLabelClass!}" style="font-weight: 400;">Nombre del Negocio</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-right:0; margin-top: 10px;">
                    <input type="text" id="user.attributes.company" class="${properties.kcInputClass!}" name="user.attributes.company" />
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <label for="user.attributes.partner" class="${properties.kcLabelClass!}" style="font-weight: 400;">Si recibes apoyo de un Socio Certificado de AVI, selecciona su nombre en la siguiente lista:</label>
                </div>

                <div class="col-sm-10 col-md-10" style="margin-top: 10px; color: #242E42">
                    <div style="position: relative">
                        <select name="user.attributes.partner" id="user.attributes.partner" style="padding: 13px 20px; border: none; border-radius: 10px; font-family: Geomanist; -moz-appearance: none; -webkit-appearance: none; letter-spacing: 1px; width: 100%; background-color: #FFFFFF;">
                            <option selected value="na">Selecciona una opción</option>
                            <option value="fryla"> Argentina - Fryla</option>
                            <option value="modeonmandd">Argentina - Mode On M&amp;D</option>
                            <option value="valohublab">Argentina - Valo Hublab</option>
                            <option value="sucrm">Argentina - SUCRM</option>
                            <option value="teledisca">Colombia - Teledisca</option>
                            <option value="hub_consulting">Colombia - Hub Consulting</option>
                            <option value="pronostica">Colombia - Pronostica</option>
                            <option value="binariadigital">Costa Rica - Binaria Digital</option>
                            <option value="doubledigit">Costa Rica - DoubleDigit</option>
                            <option value="incompanycr">Costa Rica -  Incompany CR </option>
                            <option value="prismaconsulting">Costa Rica - Prisma Consulting</option>
                            <option value="roiker">Costa Rica - Roiker</option>
                            <option value="sm_marketing">Costa Rica - SM Marketing</option>
                            <option value="tellinginteractive">Costa Rica - Telling Interactive</option>
                            <option value="inteligenciadigital4sale">Chile - Inteligencia Digital 4Sale</option>
                            <option value="adninteractive">México - ADN Interactive</option>
                            <option value="comcam">Mexico - Comcam</option>
                            <option value="mariachiio">México - MARIACHI.IO</option>
                            <option value="centria_sa">Panamá - Centria S.A</option>
                            <option value="tueresexito">Perú - TuEresExito</option>
                            <option value="kapuce_digital">Venezuela - Kapuce Digital</option>
                        </select>
                        <span class="lnr lnr-chevron-down" style="position: absolute; right: 13px; top: 14px;"></span>
                    </div>
                </div>
            </div>


            <div class="form-group" style="display: none;">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="user.attributes.parent" class="${properties.kcLabelClass!}">Parent</label>
                </div>

                <div class="col-sm-10 col-md-10">
                    <input type="text" checked class="${properties.kcInputClass!}"  id="user.attributes.parent" name="user.attributes.parent" value="true"/>
                </div>
            </div>

            <#if recaptchaRequired??>
            <div class="form-group">
                <div class="${properties.kcInputWrapperClass!}">
                    <div class="g-recaptcha" data-size="compact" data-sitekey="${recaptchaSiteKey}"></div>
                </div>
            </div>
            </#if>

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <span><a href="${url.loginUrl}">${msg("backToLogin")?no_esc}</a></span>
                </div>

                <div id="kc-form-buttons" class="col-xs-6 col-sm-6 col-md-6 col-lg-6" style="padding-right: 0;">
                    <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg("doRegister")}" style="float: right; font-weight: 400 !important;"/>
                </div>
            </div>
        </form>
    </#if>
</@layout.registrationLayout>